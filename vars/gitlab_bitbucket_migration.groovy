#!/bin/groovy

def call (Map config) {
    sh "rm -rf migration"
    
    def repos=[]
    def gRepoCommitCount=0
    def gTagCount=0
    def gBranchCount=0

    def totalPages = sh(returnStdout:true, script: """curl --head --header "PRIVATE-TOKEN: b568wxz6uT9Cb4uttKDC" "https://gitlab.com/api/v4/projects/?simple=yes&private=true&per_page=100&owned=yes" | grep x-total-pages | grep -o -e [0-9]""").trim()
    println "${totalPages}"
    int pageCount = Integer.parseInt(totalPages)

    for (i=1; i<=pageCount; i++) {
        tempRepos = sh(returnStdout:true, script: """curl -X GET --header "PRIVATE-TOKEN: b568wxz6uT9Cb4uttKDC" "https://gitlab.com/api/v4/projects/?simple=yes&private=true&per_page=100&page=${i}&owned=yes" --header "Content-Type: application/json" | jq '.[].http_url_to_repo'""")
        repos.add(tempRepos.split("\\n"))
    }
    println "${repos}"
    
}
   