#!/bin/groovy

def call(Map config = [:]){
    issueID = sh returnStdout: true, script: """git log master --merges -1 |rev|cut -d/ -f1|rev|grep -o -E '[A-Z]+-[0-9]+'|sort -u"""
    println ("${issueID}")
    return issueID
}
