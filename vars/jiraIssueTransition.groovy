#!/bin/groovy
/*
Replace Authorization: Basic ${personal_token_base_64_encoded} (can use private token also)
*/
def call (Map config = [:]) {
    if ((!config.ISSUE_ID)) {
        println ('[ERROR] Jira Issue ID/Key is missing')
        return
    }
    if ((!config.STATUS)) {
        println ('[ERROR] Jira Issue Status is missing')
        return
    }
    def getStatus = "${config.STATUS}".toLowerCase()
    List transitionId = []
    List transitionNames = []
    def transIdNames = [:]
    def jiraTransId = sh (
        script: """
        curl -H 'Authorization: Basic Y2hlMTAuZGV2b3BzQGdtYWlsLmNvbTpCc09oQzJkYkc3SmtrSWZKdFVoNzBFMDY=' 'https://che10-devops.atlassian.net/rest/api/3/issue/${config.ISSUE_ID}/transitions' -H 'Content-Type: application/json' | jq '.transitions[].id'
        """,
        returnStdout: true
    ).trim()
    transitionId = jiraTransId.replaceAll('"','').split('\n')
    def jiraTransNames = sh (
        script: """
        curl -H "Authorization: Basic Y2hlMTAuZGV2b3BzQGdtYWlsLmNvbTpCc09oQzJkYkc3SmtrSWZKdFVoNzBFMDY=" "https://che10-devops.atlassian.net/rest/api/3/issue/${config.ISSUE_ID}/transitions" -H "Content-Type: application/json" | jq '.transitions[].name'
        """,
        returnStdout: true
    ).trim().toLowerCase()
    transitionNames = jiraTransNames.replaceAll('"','').split('\n')
    for(int i = 0;i<transitionId.size();i++) {
    transIdNames.put("${transitionNames.getAt(i)}", transitionId.getAt(i))
    }
    def transition = transIdNames.get("${getStatus}")
    sh """
    curl --request POST --url "https://che10-devops.atlassian.net/rest/api/3/issue/${config.ISSUE_ID}/transitions" -H "Authorization: Basic Y2hlMTAuZGV2b3BzQGdtYWlsLmNvbTpCc09oQzJkYkc3SmtrSWZKdFVoNzBFMDY=" -H 'Accept: application/json' -H "Content-Type: application/json" --data '{ "transition": { "id": "${transition}" } }'
    """
    /*
    withEnv(['JIRA_SITE=Jira-Test']) {  //Either Hard code or provide Jira Site in the shared library.
      def transitionInput =
      [
          transition: [
              id: "${transition}"
          ]
      ]

      jiraTransitionIssue idOrKey: "${config.ISSUE_ID}", input: transitionInput
    }*/
}